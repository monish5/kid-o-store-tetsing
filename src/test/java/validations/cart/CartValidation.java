package validations.cart;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import validations.cart.constants.CartConstants;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CartValidation implements CartConstants {
    WebDriver driver;
    HashMap<String, Object> allCartItemsData;

    //    WebDriverWait wait;
//    Actions actions;
//    HashMap<String,Integer> items;
    public CartValidation(WebDriver driver) {
        this.driver = driver;
//        wait=new WebDriverWait(driver, Duration.ofSeconds(20));
//        actions= new Actions(driver);
    }

    public void validateProductName(String actualProductName) {
        String extractedProductName = driver.findElement(By.xpath(PRODUCT_NAME.replace("product_name", actualProductName))).getText();
        Assert.assertTrue(extractedProductName.equalsIgnoreCase(actualProductName));
    }

    public void validateProductVariant(String actualProductName, String actualProductVariant) {
        String extractedVariant = driver.findElement(By.xpath(PRODUCT_VARIANT.replace("productName", actualProductName))).getText().split(":")[1].trim();
        Assert.assertEquals(extractedVariant, actualProductVariant);
    }

    public void validateProductQuantity(String actualProductName, int actualQuantity) {
        String formattedProductQuantity = PRODUCT_QUANTITY.replace("productName", actualProductName);
        int extractedQuantity = Integer.parseInt(driver.findElement(By.xpath(formattedProductQuantity)).getAttribute("value"));
        Assert.assertEquals(extractedQuantity, actualQuantity);
    }

    public void validateProductPrice(String actualProductName, float actualProductPrice) {
        String formattedProductPrice = PRODUCT_PRICE.replace("productName", actualProductName);
        float extractedProductPrice = Float.parseFloat(driver.findElement(By.xpath(formattedProductPrice)).getAttribute("value"));
        Assert.assertEquals(extractedProductPrice, actualProductPrice);
    }

    public void validateTotal(String actualProductName, float actualPrice, int actualQuantity) {
        String formattedProductTotalXPath = PRODUCT_TOTAL.replace("productName", actualProductName);
        float calculatedTotal = actualPrice * (float) actualQuantity;
        float extractedTotal = Float.parseFloat(driver.findElement(By.xpath(formattedProductTotalXPath)).getText());
        Assert.assertEquals(extractedTotal, calculatedTotal);
    }

    public void emptyCart() {
        List<WebElement> removeItemButtons = driver.findElements(REMOVE_ITEM_BUTTON);
        for (int i = 0; i < removeItemButtons.size(); i++) {
            removeItemButtons.get(i).click();
        }
    }

    public void storeAllCartItems() {
        allCartItemsData = new HashMap<>();
        List<WebElement> cartItems = driver.findElements(CART_ITEMS);
        List<String> productNames = new ArrayList<>();
        List<Float> productTotals = new ArrayList<>();
        List<String> productQuantities = new ArrayList<>();

        for (int i = 0; i < cartItems.size(); i++) {
            productNames.add(cartItems.get(i).findElement(By.cssSelector(".product-name a")).getText().trim());
            productTotals.add(Float.parseFloat(cartItems.get(i).findElement(By.cssSelector("product-subtotal")).getText()));
            productQuantities.add(cartItems.get(i).findElement(By.cssSelector(".product-quantity div input")).getAttribute("value"));
        }

        allCartItemsData.put("productName", productNames);
        allCartItemsData.put("productTotal", productTotals);
        allCartItemsData.put("productTotal", productTotals);
    }

    public HashMap<String, Object> getAllCartItemsData() {
        return allCartItemsData;
    }

    public void validateSubTotal(List<Float> productTotals) {
        float calculatedTotal = 0;
        for (int i = 0; i < productTotals.size(); i++) {
            calculatedTotal = calculatedTotal + productTotals.get(i);
        }
        float extractedTotal = Float.parseFloat(driver.findElement(SUB_TOTAL).getText());
        Assert.assertEquals(extractedTotal, calculatedTotal);
    }

    public void clickOnCheckOutButton() {
        driver.findElement(CHECKOUT_BUTTON).click();
    }
}



//    public void itemsPresentInCart(){
//        System.out.println("CAlled items present in cart");
//        actions.moveToElement(driver.findElement(CartConstants.CART_ICON));
//        actions.perform();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".cart_list")));
//        List<WebElement> itemsInCart = driver.findElements(CartConstants.list);
////        List<WebElement> itemsInCart2 = driver.findElements(By.cssSelector(".cart_list"));
//        System.out.println(itemsInCart.size());
////        System.out.println(itemsInCart2.size());
//        for(int i=0;i<itemsInCart.size();++i){
//            WebElement item=itemsInCart.get(i);
//            String bookName=item.findElement(By.xpath("//a[@class='product-info']")).getText().split("Pages")[0];
//            int quantity=Integer.parseInt(item.findElement(By.xpath("//span[@class='cart_quantity']")).getText().split(" ")[0]);
////            System.out.println(bookName+" and qty is "+quantity);
//            items.put(bookName,quantity);
//        }
//    }
