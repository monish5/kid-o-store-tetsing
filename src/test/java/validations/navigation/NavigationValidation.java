package validations.navigation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import validations.navigation.constants.NavigationConstants;

import java.time.Duration;

public class NavigationValidation implements NavigationConstants{
    WebDriver driver;
    WebDriverWait wait;
    Actions actions;

    public NavigationValidation(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        actions = new Actions(driver);
    }

    public void validateLogoutButtonIsVisible(){
        System.out.println("checking logout on navigate");
        WebElement userIcon = driver.findElement(NavigationConstants.USER_ICON);
        actions.moveToElement(userIcon);
        actions.perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(NavigationConstants.LOGOUT_BUTTON));
        boolean isLogOutButtonVisible = driver.findElement(NavigationConstants.LOGOUT_BUTTON).isDisplayed();
        Assert.assertTrue(isLogOutButtonVisible,NavigationConstants.LOGOUT_BUTTON_NOT_VISIBLE_MESSAGE);
    }
    public int getCartItemCount(){
        int cartItemCount=Integer.parseInt(driver.findElement(CART_ITEM_COUNT).getText());
        return cartItemCount;
    }
    public void goToCart(){
        WebElement cartIcon=driver.findElement(CART_ICON);
        actions.moveToElement(cartIcon);
        actions.perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(VIEW_CART_BUTTON));
        driver.findElement(VIEW_CART_BUTTON).click();
    }
    public void goToHome(){
        driver.findElement(HOME).click();
    }
    public void clickOnSearchIcon(){
        driver.findElement(SEARCH_ICON).click();
    }
}
