package validations.login;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.bidi.log.Log;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import validations.login.constants.LoginConstants;

import java.time.Duration;

public class LoginValidation {

    WebDriver driver;
    WebDriverWait wait;

    public LoginValidation(WebDriver driver){
        this.driver=driver;
        wait=new WebDriverWait(driver, Duration.ofSeconds(20));
    }

    public void visitLoginPage(){
        driver.get(LoginConstants.LOGIN_URL);
    }

    public void enterUserName(String username){
        driver.findElement(LoginConstants.USERNAME_INPUT).sendKeys(username);
    }
    public void enterPassword(String password){
        driver.findElement(LoginConstants.PASSWORD_INPUT).sendKeys(password);
    }
    public void clickOnLoginButton(){
        driver.findElement(LoginConstants.SUBMIT_BUTTON).click();
    }

    public boolean checkForInvalidFeedBack(){
        return driver.findElement(LoginConstants.REQUIRED_FIELD).isDisplayed();
    }
//    public boolean checkForLoginUrl(){
//        String URL=driver.getCurrentUrl();
//        if(URL==)
//    }


}
