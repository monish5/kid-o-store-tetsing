package validations.loader.constants;

import org.openqa.selenium.By;

public class LoaderConstants {
    public static final By LOADER = By.cssSelector(".preloader");
}
