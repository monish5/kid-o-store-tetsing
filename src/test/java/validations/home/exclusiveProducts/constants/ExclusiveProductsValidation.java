package validations.home.exclusiveProducts.constants;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import validations.home.exclusiveProducts.constants.ExclusiveProductConstants;
import utils.DriverUtils;

import java.time.Duration;
import java.util.concurrent.ThreadLocalRandom;

public class ExclusiveProductsValidation implements ExclusiveProductConstants {
    WebDriver driver;
    WebDriverWait wait;
    Actions actions;
    public ExclusiveProductsValidation(WebDriver driver){
        this.driver=driver;
        wait=new WebDriverWait(driver, Duration.ofSeconds(20));
        actions= new Actions(driver);
    }

    public void clickOnStudyMaterialTab() throws InterruptedException {
        System.out.println("step 1");
        WebElement studyMaterialTab=driver.findElement(ExclusiveProductConstants.STUDY_MATERIAL_TAB);
        System.out.println(studyMaterialTab.getText());
        System.out.println("step 2");
        DriverUtils.scrollElementToCenter(studyMaterialTab,driver);
        System.out.println("step 3");
        studyMaterialTab.click();
        System.out.println("step 4");
    }

    public void clickOnUniformsTab() throws InterruptedException {
        WebElement uniformsTab=driver.findElement(ExclusiveProductConstants.UNIFORMS_TAB);
        DriverUtils.scrollElementToCenter(uniformsTab,driver);
        uniformsTab.click();
    }

    public void clickOnBooksTab() throws InterruptedException {
        WebElement booksTab=driver.findElement(ExclusiveProductConstants.BOOKS_TAB);
        DriverUtils.scrollElementToCenter(booksTab,driver);
        booksTab.click();
    }
    public void clickOnNotebooksTab() throws InterruptedException {
        WebElement notebooksTab=driver.findElement(ExclusiveProductConstants.NOTEBOOKS_TAB);
        DriverUtils.scrollElementToCenter(notebooksTab,driver);
        notebooksTab.click();
    }

    public void validateNumberOfFeaturedProductsOnStudyMaterialTab(){
        int numberOfFeaturedProducts=driver.findElements(ExclusiveProductConstants.STUDY_MATERIAL_FEATURED_PRODUCTS).size();
        Assert.assertEquals(numberOfFeaturedProducts,NUMBER_OF_PRODUCTS_TO_BE_FEATURED_ON_STUDY_MATERIAL_TAB,WRONG_NUMBER_OF_PRODUCTS_FEATURED_ON_STUDY_MATERIAL_TAB);
    }

    public void openRandomProduct() throws InterruptedException {
        int productCartNumber= ThreadLocalRandom.current().nextInt(1,NUMBER_OF_PRODUCTS_TO_BE_FEATURED_ON_STUDY_MATERIAL_TAB+1);
        String formattedProductCartViewDetailsButton=PRODUCT_CARD_VIEW_DETAIL_BUTTON.replace("numberToReplace",Integer.toString(productCartNumber));
        DriverUtils.scrollElementToCenter(driver.findElement(By.xpath(formattedProductCartViewDetailsButton)),driver);
        actions.moveToElement(driver.findElement(By.xpath(formattedProductCartViewDetailsButton)));
        actions.perform();
        driver.findElement(By.xpath(formattedProductCartViewDetailsButton)).click();
    }



}
