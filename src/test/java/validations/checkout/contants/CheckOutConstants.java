package validations.checkout.contants;

import org.openqa.selenium.By;

public interface CheckOutConstants {
    By ORDER_PRODUCTS=By.cssSelector(".order_table table tbody tr");
    By TABLE_PRODUCT_NAME=By.xpath("//td[1]");
}
