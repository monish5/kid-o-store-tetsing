package validations.checkout;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import validations.checkout.contants.CheckOutConstants;

import java.util.HashMap;
import java.util.List;

public class CheckOutValidation implements CheckOutConstants {
    WebDriver driver;
    public CheckOutValidation(WebDriver driver){
        this.driver=driver;
    }
    public void validateProductListSubTotalAndTotal(HashMap<String,Object> cartItems){
        List<WebElement> productList=driver.findElements(ORDER_PRODUCTS);
        List<String> cartProductNames=(List<String>)cartItems.get("product_names");
        List<String> cartProductQuantities = (List<String>) cartItems.get("product_quantities");
        List<Float> cartProductTotals=(List<Float>) cartItems.get("product_totals");

        float calculatedTotal = 0;
        for (int i=0;i<productList.size();i++){
            String extractedProductName = productList.get(i).findElement(TABLE_PRODUCT_NAME).getText().split("x")[0].trim();
        }
    }
}
