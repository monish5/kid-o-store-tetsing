package testrunner;

import io.cucumber.junit.Cucumber;
import io.cucumber.testng.CucumberOptions;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.testng.annotations.DataProvider;

//@RunWith Junit ka h jo @Test ki tarah hi hota h
@RunWith(Cucumber.class)
@CucumberOptions(
//        yah pe feature file and jo b test run krne h uska definition h
        features = {"src/test/resources/features"},
        glue = {"stepdefinitions"}
)
// Ye jo class ko extend kia ye saare test ko le k aaeghi TestNG se and cucember usko run krvaega
public class TestRunner extends AbstractTestNGCucumberTests {
    //yah pe isko override isliye kia kyuki humne data provider me val pass krni thi
    @Override
    @DataProvider(parallel = true)
    public Object[][] scenarios(){
        return super.scenarios();
    }

}
