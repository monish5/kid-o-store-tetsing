package tests.login;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import validations.loader.LoaderValidation;
import validations.login.LoginValidation;
import validations.login.constants.LoginConstants;
import validations.navigation.NavigationValidation;

public class LoginTests {
    WebDriver driver;
    LoginValidation loginValidation;
    LoaderValidation loaderValidation;
    NavigationValidation navigationValidation;

    public LoginTests(WebDriver driver){
        this.driver=driver;
        loginValidation=new LoginValidation(driver);
        loaderValidation= new LoaderValidation(driver);
        navigationValidation=new NavigationValidation(driver);
    }

    public void visitLoginPage(){
        loginValidation.visitLoginPage();
    }

    public void enterUsernamePasswordAndSubmit(String username,String password){
        loaderValidation.waitForLoaderToBeInvisible();
        loginValidation.enterUserName(username);
        loginValidation.enterPassword(password);
        loginValidation.clickOnLoginButton();
    }
    public void validateSuccessfulLogin(){
        loaderValidation.waitForLoaderToBeInvisible();
        navigationValidation.validateLogoutButtonIsVisible();
    }
    public void validateUnSuccessFulLogin(){
        loginValidation.checkForInvalidFeedBack();
    }
    public void validateUnSuccessFulLoginRedirectedToSamePage(){
        String url=driver.getCurrentUrl();
        Assert.assertEquals(url, LoginConstants.LOGIN_URL,"REDIRECTED TO WRONG PAGE AFTER LOGIN WITH INVALID USERNAME AND PASSWORD");
    }

}
