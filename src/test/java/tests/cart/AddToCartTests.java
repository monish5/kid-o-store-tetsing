package tests.cart;

import org.openqa.selenium.WebDriver;
import validations.cart.CartValidation;
import validations.home.exclusiveProducts.constants.ExclusiveProductsValidation;
import validations.loader.LoaderValidation;
import validations.navigation.NavigationValidation;
import validations.product.SingleProductValidation;

import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;

public class AddToCartTests {
    WebDriver driver;
    ExclusiveProductsValidation exclusiveProducts;
    LoaderValidation loader;
    SingleProductValidation singleProduct;
    NavigationValidation navigation;
    HashMap<String,Object> productData;
    CartValidation cart;
    public AddToCartTests(WebDriver driver){
        this.driver=driver;
        exclusiveProducts = new ExclusiveProductsValidation(driver);
        loader = new LoaderValidation(driver);
        singleProduct = new SingleProductValidation(driver);
        navigation = new NavigationValidation(driver);
        cart =new CartValidation(driver);
//        productData=new HashMap<>();
    }

    public void openDetailViewOfOneProduct() throws InterruptedException {
        exclusiveProducts.openRandomProduct();
    }

    public void setQuantityAndAddProductToCart() {
        loader.waitForLoaderToBeInvisible();
        singleProduct.setQuantity();
        singleProduct.storeProductData();
        singleProduct.clickOnAddToCartButton();
        singleProduct.validateSuccesfullyAddedToCartMessage();
        navigation.goToCart();
    }
//    public void checkForProductInCart(){
//        cartValidation.itemsPresentInCart();
//    }

    public void openCartAndValidateAddedProduct(){
        productData = singleProduct.getProductData();
        String actualProductName=productData.get("name").toString();
        navigation.goToCart();
        loader.waitForLoaderToBeInvisible();
        cart.validateProductName(actualProductName);
        cart.validateProductVariant(actualProductName,productData.get("variant").toString());
        cart.validateProductPrice(actualProductName,(int)productData.get("quantity"));
        cart.validateTotal(actualProductName,(float) productData.get("price"),(int)productData.get("quantity"));
    }

    public void emptyCart(){
        if(navigation.getCartItemCount()>0){
            navigation.goToCart();
            loader.waitForLoaderToBeInvisible();
            cart.emptyCart();
            navigation.goToHome();
        }
    }

}
