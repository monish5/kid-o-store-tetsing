package tests.search;

import org.openqa.selenium.WebDriver;
import validations.loader.LoaderValidation;
import validations.navigation.NavigationValidation;
import validations.search.SearchModalValidation;
import validations.search.SearchResultValidation;

public class SearchProductTests {
    WebDriver driver;
    LoaderValidation loader;
    NavigationValidation navigation;
    SearchModalValidation searchModal;
    SearchResultValidation searchResult;

    public SearchProductTests(WebDriver driver){
        this.driver=driver;
        loader=new LoaderValidation(driver);
        navigation= new NavigationValidation(driver);
        searchModal=new SearchModalValidation(driver);
        searchResult=new SearchResultValidation(driver);
    }

    public void searchAndValidateFetchedCategory(String searchValue){
        loader.waitForLoaderToBeInvisible();
        navigation.clickOnSearchIcon();
        searchModal.search(searchValue);
        loader.waitForLoaderToBeInvisible();
        searchResult.validateSearchResultCategory(searchValue);
    }
}
