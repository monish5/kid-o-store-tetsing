package tests.e2e;

import io.cucumber.java.hu.Ha;
import org.openqa.selenium.WebDriver;
import validations.cart.CartValidation;
import validations.loader.LoaderValidation;
import validations.navigation.NavigationValidation;
import validations.product.SingleProductValidation;

import java.util.HashMap;
import java.util.List;

public class CheckOutTests {
    WebDriver driver;
    LoaderValidation loader;
    CartValidation cart;
    NavigationValidation navigation;
    SingleProductValidation singleProduct;
    HashMap<String,Object> allCartItemsData;

    public CheckOutTests(WebDriver driver){
        this.driver=driver;
        loader=new LoaderValidation(driver);
        navigation=new NavigationValidation(driver);
        singleProduct=new SingleProductValidation(driver);
        cart=new CartValidation(driver);
    }

    public void addedProductToCartAndCheckedOut(){
        loader.waitForLoaderToBeInvisible();
        singleProduct.setQuantity();
        singleProduct.clickOnAddToCartButton();
        navigation.goToCart();
        loader.waitForLoaderToBeInvisible();
        cart.storeAllCartItems();
        allCartItemsData=cart.getAllCartItemsData();
        cart.validateSubTotal((List<Float>)allCartItemsData.get("product_totals"));
        cart.clickOnCheckOutButton();
    }

}
