package stepdefinitions.cart;

import factory.DriverFactory;
import io.cucumber.java.bs.A;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import tests.cart.AddToCartTests;

public class AddToCartStepDefinitions {
    WebDriver driver;
    AddToCartTests addToCartTests;

    public AddToCartStepDefinitions(){
        this.driver= DriverFactory.getDriver();
        addToCartTests=new AddToCartTests(driver);
    }
    @Then("I opened detailed view of one product from study material tab")
    public void openedDetailViewOfOneProductFromStudyMaterialTab() throws InterruptedException {
        addToCartTests.openDetailViewOfOneProduct();
    }
    @And("Increased quantity and added product to cart")
    public void increaseQuantityAndAddedProductToCart(){
        addToCartTests.setQuantityAndAddProductToCart();
    }

    @Then("Opened cart and checked the added product")
    public void openedCartAndCheckedTheAddedProduct(){
        addToCartTests.openCartAndValidateAddedProduct();
    }

    @And("Empty Cart")
    public void emptyCart(){
        addToCartTests.emptyCart();
    }
//    @Then("I checked for Successfully Added To Cart")
//    public void checkForProductAddedToCart(){
//        addToCartTests.checkForProductInCart();
//    }
//    @And("I checked for already present items in cart")
//    public void checkAlreadyPresentItemsInCart(){
//
//    }
//
//    @Then ("I changed the quantity to some negative number")
//    public void changeQuantityToNegativeAndAddedProductToCart(){
//
//    }
//
//
//    @And ("I checked that the item with negative number of units is not added in cart")
//    public void checkThatNoNegativeItemsAreAddedInCart(){
//
//    }


}
