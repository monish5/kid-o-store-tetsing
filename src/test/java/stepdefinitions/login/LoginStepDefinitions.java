package stepdefinitions.login;

import factory.DriverFactory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import tests.login.LoginTests;

public class LoginStepDefinitions {
    WebDriver driver;
    LoginTests loginTests;

    public LoginStepDefinitions(){
        driver= DriverFactory.getDriver();
        loginTests=new LoginTests(driver);
    }
    @Given("I visited login page")
    public void iVisitedLoginPage(){
        loginTests.visitLoginPage();
    }
    @Given("I logged in with {string} username and {string} password")
    public void iLoggedInWith(String username,String password){
        System.out.println("Inside Last login step");
        loginTests.enterUsernamePasswordAndSubmit(username,password);
    }
    @Then("I can see my profile icon")
    public void iCanSeeMyProfileIcon(){
        System.out.println("going for profile check");
        loginTests.validateSuccessfulLogin();
    }
    @Then("I can see the invalid or required field details")
    public void iLoggedInWithIncompleteCredentials(){
        loginTests.validateUnSuccessFulLogin();
    }

    @Then("I am redirected on the same login page")
    public void iLoggedInWithWrongCredentials(){
        loginTests.validateUnSuccessFulLoginRedirectedToSamePage();
    }
}
