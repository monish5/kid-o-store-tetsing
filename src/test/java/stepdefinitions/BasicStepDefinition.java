package stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class BasicStepDefinition {
    @Given("I visited {string}")
    public void iVisited(String URL){
        System.out.println(URL);
    }
    @Given("First cucumber code")
    public void basicStep(){
        System.out.println("Hello");
    }

    @When("I logged in with {string} username and {int} password")
    public void iLoggedInWith(String username,int password){
        System.out.println(username);
        System.out.println(password);
    }

    @Then("I click on some button")
    public void iClickedOnSomeButton(){
        System.out.println("I Clicked on Some Button");
    }
}
