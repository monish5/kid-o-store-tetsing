package stepdefinitions.hooks;

import factory.DriverFactory;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;

public class ApplicationHooks {
    WebDriver driver;
    DriverFactory driverFactory= new DriverFactory();;

    @Before
    public void openBrowser(){
        driverFactory.initDriver();
    }
    @After
    public void tearDown(){
//        DriverFactory.getDriver().quit();
    }
}
