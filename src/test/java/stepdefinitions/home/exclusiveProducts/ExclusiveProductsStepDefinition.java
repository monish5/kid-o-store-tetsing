package stepdefinitions.home.exclusiveProducts;

import factory.DriverFactory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import tests.home.exclusiveProducts.ExclusiveProductsTests;

public class ExclusiveProductsStepDefinition {
    WebDriver driver;
    ExclusiveProductsTests exclusiveProductsTests;

    public ExclusiveProductsStepDefinition(){
        this.driver= DriverFactory.getDriver();
        exclusiveProductsTests= new ExclusiveProductsTests(driver);
    }

    @Given("I opened study material tab")
    public void openStudyMaterialTab() throws InterruptedException {
        exclusiveProductsTests.openStudyMaterialTab();
    }

    @Then("Checked only required number of products are featured on study material tab")
    public void validateNumberOfProductsFeaturedOnStudyMaterialTab(){
        exclusiveProductsTests.validateNumberOfProductsFeaturedOnStudyMaterialTab();
    }
}
