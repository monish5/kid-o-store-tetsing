Feature: Checkout flow
  Background: User logged in
    Given I visited login page
    Then I logged in with "user27@gmail.com" username and "abcd1234" password

    Scenario: Checkout
      Given I opened study material tab
      Then I opened detailed view of one product from study material tab
      And Added product to cart and checked out
      Then Checked the products shown in cart table