@AddToCart
 Feature: Adding product To cart

   Background: User is on home page
     Given I visited login page
     Then I logged in with "user27@gmail.com" username and "abcd1234" password
     And Empty Cart

     Scenario: Adding product to cart
       Given I opened study material tab
       Then I opened detailed view of one product from study material tab
       And Increased quantity and added product to cart
       Then Opened cart and checked the added product
