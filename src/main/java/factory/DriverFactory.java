package factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;

public class DriverFactory {
    public static ThreadLocal<WebDriver> tl= new ThreadLocal<>();
    public ChromeOptions options;

    public DesiredCapabilities desiredCapabilities;
    public void initDriver(){
        System.setProperty("webdriver.chrome.driver","src/test/resources/drivers/chromedriver.exe");
        options=new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        tl.set(new ChromeDriver(options));


// IF U WANT TO BE ONE WHO WANTS TO TEST
//        desiredCapabilities=new DesiredCapabilities();
//        desiredCapabilities.setAcceptInsecureCerts(true);
//        desiredCapabilities.setCapability("browserName","chrome");
//        desiredCapabilities.setCapability("platformName","Windows 11");
//        tl.set(new RemoteWebDriver(new URL("http://192.168.0.142:4444"),desiredCapabilities));

//        ANOTHER WAY ALSO

//        desiredCapabilities.setBrowserName();
//        desiredCapabilities.setPlatform();

        getDriver().manage().window().maximize();
    }
    public static WebDriver getDriver(){
        return tl.get();
    }
}
